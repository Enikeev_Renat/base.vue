import axios from 'axios'
import rules from '../configs/request'

let request = (url, data, response, error, always)=>{
	if (isLocal) console.info('request', data);

	return axios({
			method: isLocal ? 'get' : 'post',
			url: url,
			headers: {
				'Content-Type': 'multipart/form-data'
			},
			transformRequest: [function (data) {
				let fd = new FormData();

				Object.keys(data).map(function(key) {
					if (Array.isArray(data[key])) {
						fd.append(key, JSON.stringify(data[key]));
					} else {
						fd.append(key, data[key]);
					}
				});

				return fd;
			}],
			data: data
		}).then(response).catch(error).then(always);
	},

	methods = ()=>{
		let data = {};

		Object.keys(rules).map((key)=>{
			data[key] = (d, r, e, a)=>{
				let obj = rules[key].customData || {};
				obj.request = rules[key].method;

				request(rules[key].url, Object.assign(obj, d), r, e, a);
			};
		});

		return data;	// apiMethod(data, response, error, always);
	}
;

export default {
	install: (Vue, options)=>{
		Vue.prototype.$api = methods();
	}
};

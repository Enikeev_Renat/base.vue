import Vue from 'vue'
import Router from 'vue-router'


import Home from '../../view/home.vue';
import Error from '../../view/error.vue';
import Components from '../../view/components.vue';

Vue.use(Router);

const routes = [
	{
		path: '/',
		component: Home,
		meta: {
			title: 'Home'
		}
	},
	{
		path: '/components',
		component: Components,
		meta: {
			title: 'Сomponents example'
		}
	},
	{
		path: '*',
		component: Error,
		meta: {
			title: 'Страница не существует'
		}
	}
];

const router = new Router({
	mode: 'history',
	routes,
	linkActiveClass: '',
	linkExactActiveClass: 'is-active',
	transitionOnLoad: true,
	root: '/'
});


router.afterEach((to, from) => {
	document.title = to.meta.title;
});

export default router
let prodUrl = '/request';

export default {
	/**
	 * authorization
	 */
	logIn: {
		method: 'login',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	},
	logOut:  {
		method: 'logout',
		customData: {},
		url: isLocal ? '/data/login.json' : prodUrl
	}
};
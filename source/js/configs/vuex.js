import Vue from 'vue'
import Vuex from 'vuex';

Vue.use(Vuex);

export default new Vuex.Store({
	state: {
		popup: {
			name: null,
			data: null
		},

		loading: false,

		auth: {

		},

	},

	mutations: {
		// popups
		openPopup (state, options) {
			if ( typeof options === 'string') {
				state.popup.name = options;
			} else {
				state.popup.name = options.name;
				state.popup.data = options.data;
			}
			if (name !== null) document.documentElement.classList.add('is-popup-open');
		},
		closePopup (state) {
			state.popup.name = null;
			state.popup.data = null;
			document.documentElement.classList.remove('is-popup-open');
		},
		loading (state, event){
			if ( event === 'start') {
				state.loading = true;
			} else if ( event === 'end') {
				state.loading = false;
			}
		}
	},


	getters: {
		getPopupName: state => state.popup.name,
		getPopupData: state => state.popup.data,
	}
});




import style from './css/style.scss';

import Vue from 'vue';
import router from './js/configs/router.js'
import store from './js/configs/vuex.js'
import './js/configs/filters.js'

import Api from './js/modules/module.api'
Vue.use(Api);

import './js/modules/module.helper.js'

import index from './view/index.vue';

window.app = new Vue({
	el: '#app',
	filters: {},
	router,
	store,
	render: h => h(index)
});
